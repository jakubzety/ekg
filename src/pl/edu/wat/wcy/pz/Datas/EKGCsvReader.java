/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.Datas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.edu.wat.wcy.pz.DB.DB;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 *
 * @author Jakub
 */
public class EKGCsvReader {

    public static EKGData readData(String filePathName) {
        return readData(new File(filePathName));
    }

    public static EKGData readData(File file) {
        ArrayList<Double[]> lst = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = br.readLine();
            String[] tempTab;
            Double tab[] = new Double[2];
            tempTab = line.split(";");
            switch (tempTab.length) {
                case 1: // val;
                    int i = 0;
                    while ((line = br.readLine()) != null) {
                        tempTab = line.split(";");
                        tab[0] = ++i * 0.004;
                        tab[1] = Double.parseDouble(tempTab[0]) * 0.002;
                        lst.add(tab.clone());
                    }
                    break;
                case 2: // sek;val
                    while ((line = br.readLine()) != null) {
                        tempTab = line.split(";");
                        tab[0] = Double.parseDouble(tempTab[0]) * 0.004;
                        tab[1] = Double.parseDouble(tempTab[1]) * 0.001;
                        lst.add(tab.clone());
                    }
                    break;
                case 3: // sek;real;imagine 
                    Double valRe,
                     valIm;
                    while ((line = br.readLine()) != null) {
                        tempTab = line.split(";");
                        tab[0] = Double.parseDouble(tempTab[0]) * 0.004;
                        valRe = Double.parseDouble(tempTab[1]) * 0.001;
                        valIm = Double.parseDouble(tempTab[2]) * 0.001;
                        tab[1] = Math.sqrt(valRe * valRe + valIm * valIm);
                        lst.add(tab.clone());
                    }
                    break;
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EKGCsvReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EKGCsvReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new EKGData(lst);
    }
    
    public static void readDataFromDB() throws ClassNotFoundException, SQLException
    {
        DB handler = new DB();
        ResultSet rs = handler.select("SELECT * FROM pacjenci");
        while(rs.next())
        {
            String result = rs.getString("imie");
            System.out.println(result);
        }
    }
    
}
