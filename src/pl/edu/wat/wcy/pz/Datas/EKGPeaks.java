/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.Datas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jakub
 */
public class EKGPeaks {

    private static double samplingRate = 0.004; // 25 mm (250 liczb) to 1s

    private static ArrayList<Double[]> maxSearch(int winSize, EKGData data) {
        int i = 1;
        ArrayList<Double[]> rep = new ArrayList<>();
        Double[] maks = data.getPoint(0);
        for (Double[] tab : data.getData()) {
            if (i % winSize == 0) {
                rep.add(maks.clone());
                maks = tab;
            } else {
                if (Double.compare(maks[1], tab[1]) < 0) {
                    maks = tab;
                }
            }
            i++;
        }
        return rep;
    }

    private static ArrayList<Double[]> check(ArrayList<Double[]> peaks, EKGData data) {
        ArrayList<Double[]> rep = new ArrayList<>();
        double maks = data.getMaxVal();
        for (int i = 0; i < peaks.size(); i++) {
            Double[] tab = peaks.get(i);
            Double test = tab[1] / (maks / 10.0);
            if (Double.compare(test, 3.5) >= 0) {
                
                rep.add(tab.clone());
            }
        }
        /*for (int i = 0; i < peaks.size(); i++) {
            double x = peaks.get(i)[0];
            for (; data.getPoint(j)[0] < x; j++);
            double dxPlus = 0, dxMinus = 0,
                    val0 = data.getPoint(j)[1];
            if (j < data.getSize()) {
                double val1 = data.getPoint(j + 1)[1];
                dxPlus = (val1 - val0);
            }
            if (j > 0) {
                double val2 = data.getPoint(j - 1)[1];
                dxMinus = (val2 - val0);
            }
            if ((-1000000.0 * dxPlus * dxMinus) <= 0.0) { // skrócone od: ((f(x_0-h)-f(x_0))/(-h))*((f(x_0+h)-f(x_0)/(h)) <= 0 dla h=0.001
                if (dxPlus<0.0 && dxMinus <0.0)
                    rep.add(peaks.get(i).clone());
            } else {
                Double[] tab = peaks.get(i);
                Double test = 0.0;
                test = tab[1] / (maks / 11.0);
                if (Double.compare(test, 5.0) >= 0) {
                    rep.add(tab.clone());
                }
            }
        }*/
        return rep;
    }

    public static EKGData findPeaks(EKGData data) {
        double[] AR = data.getXTab().clone(),
                VAL = data.getValTab().clone();
        FFTCalculator.clearSig(VAL);
        List<Double[]> lst = new ArrayList<>();
        for (int i = 0; i < AR.length; i++) {
            Double[] tab = {AR[i], VAL[i]};
            lst.add(tab);
        }
        EKGData fftresult = new EKGData(lst);

        int windowSize = Math.round(Math.round(0.840/samplingRate));
        ArrayList<Double[]> peaks = maxSearch(windowSize, fftresult);
        //TODO: nie zapominj dodać filtrowania! 
        peaks = check(peaks, fftresult);
        //
        if (peaks.isEmpty()) {
            return null;
        }
        Double[] x = peaks.get(0);
        int minDist = fftresult.getSize();
        double minVal = fftresult.getLength();
        for (int i = 1; i < peaks.size(); i++) {
            Double[] y = peaks.get(i);
            if (Double.compare(minVal, (y[0] - x[0])) > 0) {
                if(Math.round(Math.round((y[0] - x[0]) * 1000.0))>30){
                    minVal = y[0] - x[0];
                    minDist = Math.round(Math.round((y[0] - x[0]) * 1000.0));
                }
            }
            x = y;
        }
        
        peaks = maxSearch(Math.round(Math.round((minDist * 1.2))), fftresult);
        //TODO: nie zapominj dodać filtrowania! 
        peaks = check(peaks, fftresult);
        x = peaks.get(0);
        for (int i = 1; i < peaks.size(); i++) {
            Double[] y = peaks.get(i);
            if (Double.compare(data.getVal(y[0]),0.0)<0){
                peaks.remove(y);
                y = peaks.get(i);
            }
            if (Double.compare(minVal, (y[0] - x[0])) > 0) {
                if(Math.round(Math.round((y[0] - x[0]) * 1000.0))<60){
                    if(Double.compare(y[1],x[1])>0){
                        peaks.remove(x);
                        if(i>2){
                            x = peaks.get(i-2);
                            i--;
                        }
                    }else{
                        peaks.remove(y);
                        if(i>2){
                            i--;
                        }
                    }
                }
            }
            x = y;
        }
        //
        System.out.println("dist : " + minDist);        
        System.out.println("peaks : " + peaks.size());
        System.out.println("ratio : " + ((double) peaks.size()) / ((data.getSize() * samplingRate) / 60.0) + "/min");
        return new EKGData((List<Double[]>) peaks);
    }
}
