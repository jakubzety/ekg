/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.wat.wcy.pz.Datas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jakub
 */
public class EKGCsvWriter {
    public static void writeData(String filename,EKGData data){
        writeData(new File(filename),data);
    }
    public static void writeData(File file,EKGData data){
        try {
            if(!file.canWrite()){
                file.createNewFile();
            }
            BufferedWriter wr = new BufferedWriter(new FileWriter(file));
            String rep = "";
            double[] ar = data.getXTab(), val = data.getValTab();
            for(int i=0;i<data.getSize();i++){
                rep = Integer.toString(Math.round(Math.round( ar[i]*1000.0)))+";"
                     +Integer.toString(Math.round(Math.round(val[i]*1000.0)));
                wr.write(rep, 0,rep.length());
                wr.newLine();
            }
            wr.close();
        } catch (IOException ex) {
            Logger.getLogger(EKGCsvWriter.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
}
