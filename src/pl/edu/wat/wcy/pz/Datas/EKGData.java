/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.Datas;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jakub
 */
public class EKGData implements Cloneable {

    private final ArrayList<Double[]> data = new ArrayList<>();
    private final int size;
    private final double length;

    public EKGData(List<Double[]> list) {
        double sumLen = 0;
        for (Object[] obj : list) {
            Double[] temp = new Double[2];
            temp[0] = Double.parseDouble(obj[0].toString());
            temp[1] = Double.parseDouble(obj[1].toString());
            sumLen = temp[0];
            data.add(temp);
        }
        this.size = list.size();
        this.length = sumLen; // ostatnia,największa wartość czasu w zestawie
    }
    
    public Double[] getPoint(int i) {
        if (i < size) {
            return data.get(i);
        } else {
            return null;
        }
    }
    
    public Double getVal(double x){
        for (Double[] tab : data){
            if (tab[0].equals(Double.valueOf(x))){
                return tab[1];
            }
        }
        return null;
    }
    
    public double getMinVal()
    {
        double rep =data.get(0)[1];
        for(Double[] tab : data ){
            if(tab[1].compareTo(rep)<0){
                rep = tab[1];
            }
        }
        return rep;
    }
    
    public double getMaxVal()
    {
        double rep = data.get(0)[1];
        for(Double[] tab : data ){
            if(tab[1].compareTo(rep)>0){
                rep = tab[1];
            }
        }
        return rep;
    }
    
    public int getSize() {
        return size;
    }

    public ArrayList<Double[]> getData() {
        return data;
    }

    public double getLength() {
        return length;
    }

    @Override
    public void finalize() {
        try {
            data.clear();
            super.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(EKGData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public double[] getXTab(){
        double[] ret = new double[data.size()];
        for(int i=0;i<data.size();i++){
            ret[i]=getPoint(i)[0];
        }
        return ret;
    }
    
    public double[] getValTab(){
        double[] ret = new double[data.size()];
        for(int i=0;i<data.size();i++){
            ret[i]=getPoint(i)[1];
        }
        return ret;
    }

    @Override
    public EKGData clone() {
        return new EKGData((List<Double[]>) data.clone());
    }
    
}
