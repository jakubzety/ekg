/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.wat.wcy.pz.Components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Jakub
 */
public class RollPane extends JPanel{
    
    private int buttonWidth = 15, panelWidth=150, rollingSpeed = 3;
    private boolean rolledUp =false,rolling=false;
    private Color buttonColor = Color.DARK_GRAY;
    private final Runnable roller;
    
    public RollPane(){
        super();
        final RollPane thiz = this;
        roller = new Runnable() {
            @Override
            public void run() {
                thiz.setRolling(true);
                try {
                    if(thiz.isRolledUp()){
                        while (thiz.getWidth()<thiz.panelWidth){
                            thiz.firePropertyChange("width", thiz.getWidth(), thiz.getWidth()+rollingSpeed);
                            thiz.setSize(thiz.getWidth()+rollingSpeed,thiz.getHeight());
                            thiz.setPreferredSize(new Dimension(thiz.getWidth()+rollingSpeed,thiz.getHeight()));
                            thiz.firePropertyChange("x", thiz.getX(), thiz.getX()-rollingSpeed);
                            thiz.setLocation(thiz.getX()-rollingSpeed,thiz.getY());
                            Thread.sleep(1);
                        }
                    }else{
                        while (thiz.getWidth()>thiz.getButtonWidth()){
                            thiz.firePropertyChange("width", thiz.getWidth(), thiz.getWidth()-rollingSpeed);
                            thiz.setSize(thiz.getWidth()-rollingSpeed,thiz.getHeight());
                            thiz.setPreferredSize(new Dimension(thiz.getWidth()+rollingSpeed,thiz.getHeight()));
                            thiz.firePropertyChange("x", thiz.getX(), thiz.getX()+rollingSpeed);
                            thiz.setLocation(thiz.getX()+rollingSpeed,thiz.getY());
                            Thread.sleep(1);
                        }
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(RollPane.class.getName()).log(Level.SEVERE, null, ex);
                }
                thiz.setRolledUp(!thiz.isRolledUp());
                thiz.setRolling(false);
            }
        };
        this.setBorder(BorderFactory.createMatteBorder(0, buttonWidth, 0, 0, buttonColor));
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getX()<buttonWidth){
                    thiz.roll();
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {}
            @Override
            public void mouseReleased(MouseEvent e) {}
            @Override
            public void mouseEntered(MouseEvent e) {}
            @Override
            public void mouseExited(MouseEvent e) {}
        });
    }
       
    public void roll(){
        if(!isRolling()){
            new Thread(roller).start();
        }
    }
    
    // GETTERS'N'SETTERS
    public int getRollingSpeed() {
        return rollingSpeed;
    }

    public void setRollingSpeed(int rollingSpeed) {
        firePropertyChange("rollingSpeed", this.rollingSpeed, rollingSpeed);
        this.rollingSpeed = rollingSpeed;
    }    
    
    public int getButtonWidth() {
        return buttonWidth;
    }

    public void setButtonWidth(int buttonWidth) {
        firePropertyChange("buttonWidth", this.buttonWidth, buttonWidth);
        this.buttonWidth = buttonWidth;
    }

    public int getPanelWidth() {
        return panelWidth;
    }

    public void setPanelWidth(int panelWidth) {
        firePropertyChange("panelWidth", this.panelWidth, panelWidth);
        this.panelWidth = panelWidth;
    }

    public boolean isRolledUp() {
        return rolledUp;
    }

    private void setRolledUp(boolean rolledUp) {
        firePropertyChange("rolledUp", this.rolledUp, rolledUp);
        this.rolledUp = rolledUp;
    }

    public boolean isRolling() {
        return rolling;
    }

    private void setRolling(boolean rolling) {
        firePropertyChange("rolling", this.rolling, rolling);
        this.rolling = rolling;
    }

    public Color getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(Color buttonColor) {
        this.buttonColor = buttonColor;
    }
        
}
