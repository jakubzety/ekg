/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.Components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JComponent;
import pl.edu.wat.wcy.pz.Datas.EKGData;
import pl.edu.wat.wcy.pz.Datas.EKGPeaks;

/**
 *
 * @author Jakub
 */
public class GraphView extends JComponent {

    private boolean showPeaks = false, showGrid = true;
    private String stateTxt ="Przeciągnij tutaj plik bądź wybierz z menu ładowania";
    private EKGData datas, peaks;
    private Color colorGrid = Color.GRAY, colorPlot = Color.RED, colorMark = Color.BLUE, colorAround = Color.BLACK, colorBCKGRND = Color.WHITE;
    private float scaleX = (float) 100.0, scaleY = (float) 100.0;
    private int splitXNum = 5, splitYNum = 5;
    private int axeXborder = 20, axeYborder = 40;
    private int margineNorth = 5, margineWest = 5, margineEast = 30, margineSouth = 5;
    private double positionX = (double) 0.0, positionY = (double) 0.0; // wartosc minimalna na osi X

    public void putData(EKGData dat) {
        firePropertyChange("datas", this.datas, dat);
        this.datas = dat.clone();
        EKGData temp = new EKGData(new ArrayList<Double[]>());
        firePropertyChange("peaks", this.peaks, temp);
        this.peaks = temp;
        this.autoScale();
        this.setStateTxt("Załadowano dane");
    }

    public void erraseData() {
        if (datas != null) {
            datas.finalize();
        }
        if (peaks != null) {
            peaks.finalize();
        }
        datas = null;
        peaks = null;
        this.setStateTxt("Przeciągnij tutaj plik bądź wybierz z menu ładowania");
        setShowPeaks(false);
        System.gc();
    }

    public void reset() {
        this.erraseData();
        colorGrid = Color.GRAY;
        colorPlot = Color.RED;
        colorMark = Color.BLUE;
        colorAround = Color.BLACK;
        colorBCKGRND = Color.WHITE;
        showPeaks = false;
        showGrid = true;
        scaleX = (float) 100.0;
        scaleY = (float) 30.0;
        splitXNum = 5;
        splitYNum = 5;
        axeXborder = 20;
        axeYborder = 40;
        margineNorth = 5;
        margineWest = 5;
        margineEast = 30;
        margineSouth = 5;
        positionX = 0.0;
        positionY = 0.0;
        this.setStateTxt("Przeciągnij tutaj plik bądź wybierz z menu ładowania");
        System.gc();
    }

    @Override
    public void paint(Graphics g) {
        super.paintComponent(g);
        g.setColor(colorBCKGRND);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        if (datas != null) { //jesli są dane wysowanie wykresu
            BufferedImage im = new BufferedImage(this.getWidthGraph(), this.getHeightGraph(), BufferedImage.TYPE_INT_RGB);
            Graphics g2 = im.getGraphics();
            g2.setColor(colorBCKGRND);
            g2.fillRect(0, 0, im.getWidth(), im.getHeight());
            g2.setColor(colorPlot);
            Double[] point1, point2;
            for (int i = 1; i < datas.getSize(); i++) {
                if (datas.getPoint(i)[0] > positionX) {
                    point2 = datas.getPoint(i - 1);
                    for (; (Math.round(Math.round((point2[0] - positionX) * scaleX))) <= (im.getWidth()); i++) {
                        point1 = point2;
                        point2 = datas.getPoint(i);
                        if (point2 != null) {
                            g2.drawLine(Math.round(Math.round((point1[0] - positionX) * scaleX)), im.getHeight() - Math.round(Math.round((point1[1] - positionY) * scaleY)),
                                    Math.round(Math.round((point2[0] - positionX) * scaleX)), im.getHeight() - Math.round(Math.round((point2[1] - positionY) * scaleY)));
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
            g.drawImage(im, axeYborder + margineWest, margineNorth, this);
        }
        //rysowanie osi
        g.setColor(colorAround);
        g.drawLine(margineWest + axeYborder, margineNorth,
                margineWest + axeYborder, this.getHeightGraph() + margineNorth);
        g.drawLine(margineWest + axeYborder, this.getHeightGraph() + margineNorth,
                this.getWidth() - margineEast, this.getHeightGraph() + margineNorth);
        int stepY = Math.round((float) (this.getHeightGraph()) / (float) (splitYNum + 1));
        for (int i = 0; i <= (splitYNum + 1); i++) {
            g.drawString(Double.toString(Math.round((positionY + ((double) ((stepY * (splitYNum + 1)) - (i * stepY)) / (double) scaleY)) * 1000.0) / 1000.0), 0, i * stepY + (g.getFont().getSize() / 2) + margineNorth);
            g.drawLine(axeYborder + margineWest - 2, i * stepY + margineNorth, axeYborder + margineWest + 2, i * stepY + margineNorth);
            if (showGrid && i != 0 && i != (splitYNum + 1)) {
                g.setColor(colorGrid);
                drawGridLine(g, axeYborder + margineWest, i * stepY + margineNorth, this.getWidthGraph() + axeYborder + margineWest, i * stepY + margineNorth, 5, 5);
                g.setColor(colorAround);
            }
        }
        int stepX = Math.round((float) (this.getWidthGraph()) / (float) (splitXNum + 1));
        for (int i = 0; i <= (splitXNum + 1); i++) {
            g.drawString(Double.toString(Math.round((positionX + ((double) (i * stepX)) / (double) scaleX) * 1000.0) / 1000.0), i * stepX + axeYborder + margineWest, this.getHeightGraph() + margineNorth + g.getFont().getSize() + 5);
            g.drawLine(margineWest + axeYborder + (i * stepX), this.getHeightGraph() + margineNorth - 2, margineWest + axeYborder + (i * stepX), this.getHeightGraph() + margineNorth + 2);
            if (showGrid && i != 0 && i != (splitXNum + 1)) {
                g.setColor(colorGrid);
                drawGridLine(g, margineWest + axeYborder + (i * stepX), margineNorth, margineWest + axeYborder + (i * stepX), this.getHeightGraph() + margineNorth, 5, 5);
                g.setColor(colorAround);
            }
        }
        //dopelnienie ramki
        g.drawLine(axeYborder + margineWest, margineNorth,
                this.getWidth() - margineEast, margineNorth);
        g.drawLine(this.getWidth() - margineEast, margineNorth,
                this.getWidth() - margineEast, this.getHeightGraph() + margineNorth);
        //rysowanie peaków
        if (showPeaks && peaks != null) {
            g.setColor(colorMark);
            if (peaks.getData().isEmpty()) {
                EKGData temp = EKGPeaks.findPeaks(datas);
                if (temp != null) {
                    setPeaks(temp);
                }
            } else {
                for (int i = 0; i < peaks.getSize(); i++) {
                    if (peaks.getPoint(i)[0] > positionX) {
                        for (; (Math.round(Math.round((peaks.getPoint(i)[0] - positionX) * scaleX))) <= (getWidthGraph()); i++) {
                            g.fillOval(Math.round(Math.round((peaks.getPoint(i)[0] - positionX) * scaleX)) + margineWest + axeYborder - 3, getHeightGraph() + 3 - Math.round(Math.round((datas.getVal(peaks.getPoint(i)[0]) - positionY) * scaleY)),
                                    6, 6);
                            if (i + 1 >= peaks.getSize()) {
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        //rysowanie mgiełki
        if(!this.isEnabled()){
            g.setColor(new Color((float)0.9,(float)0.9,(float)0.9,(float)0.8));
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(colorAround);
            g.drawChars(this.getStateTxt().toCharArray(),0,this.getStateTxt().length(),(int)((this.getWidth()/2)-(stateTxt.length()*2.8)), this.getHeight()/2);
        }
    }

    private void drawGridLine(Graphics g, int x1, int y1, int x2, int y2, int lenLine, int lenGap) {
        double absLen = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)), angle = Math.asin((x2 - x1) / absLen);
        int dxL = Math.round(Math.round((lenLine * Math.sin(angle)))),
                dyL = Math.round(Math.round((lenLine * Math.cos(angle)))),
                dxG = Math.round(Math.round((lenGap * Math.sin(angle)))),
                dyG = Math.round(Math.round((lenGap * Math.cos(angle))));
        int[] point1 = {x1, y1}, point2 = {x1 + dxL, y1 + dyL};
        while (Double.compare(Math.sqrt((point2[0] - x1) * (point2[0] - x1) + (point2[1] - y1) * (point2[1] - y1)), absLen) < 0) {
            g.drawLine(point1[0], point1[1], point2[0], point2[1]);
            point1[0] = point2[0] + dxG;
            point1[1] = point2[1] + dyG;
            point2[0] = point1[0] + dxL;
            point2[1] = point1[1] + dyL;
        }
    }

    public void autoScale() {
        this.setPositionY(this.datas.getMinVal());
        this.setScaleY(Math.round(((double) this.getHeightGraph()) / (this.datas.getMaxVal() - this.getPositionY())));
    }

    //Getters'n'Setters    
    public int getWidthGraph() {
        return super.getWidth() - margineWest - margineEast - axeYborder;
    }

    public int getHeightGraph() {
        return super.getHeight() - margineNorth - margineSouth - axeXborder;
    }

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {
        if (scaleX != 0) {
            firePropertyChange("scaleX", this.scaleX, scaleX);
            this.scaleX = scaleX;
            this.repaint();
        }
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        if (scaleY != 0) {
            firePropertyChange("scaleY", this.scaleY, scaleY);
            this.scaleY = scaleY;
            this.repaint();
        }
    }

    public double getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        firePropertyChange("positionX", this.positionX, positionX);
        this.positionX = positionX;
        this.repaint();
    }

    public int getAxeXborder() {
        return axeXborder;
    }

    public double getPositionY() {
        return positionY;
    }

    public void setPositionY(double positionY) {
        firePropertyChange("positionY", this.positionY, positionY);
        this.positionY = positionY;
        this.repaint();
    }

    public void setAxeXborder(int axeXborder) {
        firePropertyChange("axeXborder", this.axeXborder, axeXborder);
        this.axeXborder = axeXborder;
        this.repaint();
    }

    public int getAxeYborder() {
        return axeYborder;
    }

    public void setAxeYborder(int axeYborder) {
        firePropertyChange("axeYborder", this.axeYborder, axeYborder);
        this.axeYborder = axeYborder;
        this.repaint();
    }

    public void setPositionXProp(int prop, int from) {
        if (prop >= 0) {
            firePropertyChange("positionX", this.positionX, ((double) prop / (double) from) * (datas.getLength() - ((this.getWidth() - this.axeXborder) / scaleX)));
            this.positionX = ((double) prop / (double) from) * (datas.getLength() - ((this.getWidthGraph()) / scaleX));
            this.repaint();
        }
    }

    public EKGData getDatas() {
        return datas;
    }

    public EKGData getPeaks() {
        return peaks;
    }

    private void setPeaks(EKGData peaks) {
        firePropertyChange("peaks", this.peaks, peaks);
        this.peaks = peaks;
        repaint();
    }

    public int getMargineNorth() {
        return margineNorth;
    }

    public void setMargineNorth(int margineNorth) {
        firePropertyChange("margineNorth", this.margineNorth, margineNorth);
        this.margineNorth = margineNorth;
        this.repaint();
    }

    public int getMargineWest() {
        return margineWest;
    }

    public void setMargineWest(int margineWest) {
        firePropertyChange("margineWest", this.margineWest, margineWest);
        this.margineWest = margineWest;
        this.repaint();
    }

    public int getMargineEast() {
        return margineEast;
    }

    public void setMargineEast(int margineEast) {
        firePropertyChange("margineEast", this.margineEast, margineEast);
        this.margineEast = margineEast;
        this.repaint();
    }

    public int getMargineSouth() {
        return margineSouth;
    }

    public void setMargineSouth(int margineSouth) {
        firePropertyChange("margineSouth", this.margineSouth, margineSouth);
        this.margineSouth = margineSouth;
        this.repaint();
    }

    public int getSplitXNum() {
        return splitXNum;
    }

    public void setSplitXNum(int splitXNum) {
        firePropertyChange("splitXNum", this.splitXNum, splitXNum);
        this.splitXNum = splitXNum;
    }

    public int getSplitYNum() {
        return splitYNum;
    }

    public void setSplitYNum(int splitYNum) {
        firePropertyChange("splitYNum", this.splitYNum, splitYNum);
        this.splitYNum = splitYNum;
    }

    public boolean isShowPeaks() {
        return showPeaks;
    }

    public void setShowPeaks(boolean showPeaks) {
        firePropertyChange("showPeaks", this.showPeaks, showPeaks);
        this.showPeaks = showPeaks;
        this.repaint();
    }

    public boolean isShowGrid() {
        return showGrid;
    }

    public void setShowGrid(boolean showGrid) {
        firePropertyChange("showGrid", this.showGrid, showGrid);
        this.showGrid = showGrid;
    }

    public Color getColorGrid() {
        return colorGrid;
    }

    public void setColorGrid(Color colorGrid) {
        firePropertyChange("colorGrid", this.colorGrid, colorGrid);
        this.colorGrid = colorGrid;
    }

    public Color getColorPlot() {
        return colorPlot;
    }

    public void setColorPlot(Color colorPlot) {
        firePropertyChange("colorPlot", this.colorPlot, colorPlot);
        this.colorPlot = colorPlot;
    }

    public Color getColorMark() {
        return colorMark;
    }

    public void setColorMark(Color colorMark) {
        firePropertyChange("colorMark", this.colorMark, colorMark);
        this.colorMark = colorMark;
    }

    public Color getColorAround() {
        return colorAround;
    }

    public void setColorAround(Color colorAround) {
        firePropertyChange("colorAround", this.colorAround, colorAround);
        this.colorAround = colorAround;
    }

    public Color getColorBCKGRND() {
        return colorBCKGRND;
    }

    public void setColorBCKGRND(Color colorBCKGRND) {
        firePropertyChange("colorBCKGRND", this.colorBCKGRND, colorBCKGRND);
        this.colorBCKGRND = colorBCKGRND;
    }

    public String getStateTxt() {
        return stateTxt;
    }

    public void setStateTxt(String stateTxt) {
        firePropertyChange("stateTxt", this.stateTxt, stateTxt);
        this.stateTxt = stateTxt;
    }
    
    
}
