/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.wat.wcy.pz.Components.others;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @source http://www.java2s.com/Tutorial/Java/0240__Swing/SettingComboBoxEditorandComboBoxRenderer.htm
 * 
 */
public class ColorCellRenderer implements ListCellRenderer {
    final public static Color[] colors = new Color[]{
        Color.BLACK,Color.DARK_GRAY,Color.GRAY,Color.LIGHT_GRAY,Color.WHITE,
        Color.RED,Color.GREEN,Color.BLUE,Color.CYAN,Color.MAGENTA,Color.YELLOW,
        Color.ORANGE,Color.PINK};
    final private static String[] names = new String[]{
        "Czarny","Ciemno szary","Szary","Jasno szary","Biały",
        "Czerwony","Zielony","Niebieski","Seledynowy","Magenta","Żółty",
        "Pomarańczowy","Różowy"};
    private Dimension dimension = new Dimension(7, 20);
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean cellHasFocus) {
        String temp = "";
        for (int i =0;i<colors.length;i++){
            if(colors[i].equals(value)){
                temp = names [i];
                break;
            }
        }
        javax.swing.JLabel renderer = (javax.swing.JLabel) defaultRenderer.getListCellRendererComponent(list, temp.isEmpty()?value:temp , index,isSelected, cellHasFocus);
        if (value instanceof Color) {
            renderer.setBackground((Color) value);
        }
        if(cellHasFocus || isSelected){
            renderer.setBorder(new javax.swing.border.LineBorder(Color.DARK_GRAY));
        }else{
            renderer.setBorder(null);
        }
        renderer.setPreferredSize(dimension);
        return renderer;
    }
}