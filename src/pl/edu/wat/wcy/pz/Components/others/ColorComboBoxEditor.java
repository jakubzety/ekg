/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.wat.wcy.pz.Components.others;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxEditor;
import javax.swing.JColorChooser;
import javax.swing.event.EventListenerList;

/**
 * 
 * @source http://www.java2s.com/Tutorial/Java/0240__Swing/SettingComboBoxEditorandComboBoxRenderer.htm
 * 
 */
public class ColorComboBoxEditor implements ComboBoxEditor {
    final static private JColorChooser jColorChooser1 = new JColorChooser();
    final protected javax.swing.JButton editor;
    protected EventListenerList listenerList = new EventListenerList();
    public ColorComboBoxEditor(Color initialColor) {
        editor = new javax.swing.JButton("");
        editor.setBackground(initialColor);
        editor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color currentBackground = editor.getBackground();
                Color color = jColorChooser1.showDialog(editor, "Color Chooser", currentBackground);
                if ((color != null) && (currentBackground != color)) {
                  editor.setBackground(color);
                  fireActionEvent(color);
                }
            }
        });
    }
    @Override
    public void addActionListener(ActionListener l) {
        listenerList.add(ActionListener.class, l);
    }
    @Override
    public Component getEditorComponent() {
        return editor;
    }
    @Override
    public Object getItem() {
        return editor.getBackground();
    }
    @Override
    public void removeActionListener(ActionListener l) {
        listenerList.remove(ActionListener.class, l);
    }
    @Override
    public void selectAll() {
      // Ignore
    }
    @Override
    public void setItem(Object newValue) {
        if (newValue instanceof Color) {
            Color color = (Color) newValue;
            editor.setBackground(color);
        } else {
            try {
              Color color = Color.decode(newValue.toString());
              editor.setBackground(color);
            } catch (NumberFormatException ex) {
                Logger.getLogger(ColorComboBoxEditor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    protected void fireActionEvent(Color color) {
      Object listeners[] = listenerList.getListenerList();
      for (int i = listeners.length - 2; i >= 0; i -= 2) {
          if (listeners[i] == ActionListener.class) {
              ActionEvent actionEvent = new ActionEvent(editor, ActionEvent.ACTION_PERFORMED, color.toString());
              ((ActionListener) listeners[i + 1]).actionPerformed(actionEvent);
          }
      }
    }
}