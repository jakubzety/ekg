/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.edu.wat.wcy.pz.Components.others;

import java.awt.Color;
import java.awt.Component;
import java.awt.color.ColorSpace;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Jakub
 */
public class PatientCellRenderer implements ListCellRenderer{  
    private static javax.swing.border.BevelBorder borderRaise = new javax.swing.border.BevelBorder(javax.swing.border.SoftBevelBorder.RAISED);
    private static javax.swing.border.BevelBorder borderLower = new javax.swing.border.BevelBorder(javax.swing.border.SoftBevelBorder.LOWERED);
    private static javax.swing.ImageIcon iconPatient = new javax.swing.ImageIcon("./resources/clipboardPerson2.png","ikona pcjenta");
    private static javax.swing.ImageIcon iconGraph = new javax.swing.ImageIcon("./resources/clipboardEKG2.png","ikona przebiegu");
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        javax.swing.JLabel renderer = (javax.swing.JLabel) defaultRenderer.getListCellRendererComponent(list,value, index,isSelected, cellHasFocus);
        renderer.setBackground((Color.LIGHT_GRAY));
        renderer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        if(isSelected){
            renderer.setBorder(borderLower);
        }else{
            renderer.setBorder(borderRaise);
        }
        /*
        *   tu tuaj powinno być sprawdzanie czy value jest klasy Pacjent czy Przebieg i seter odpowiedznie ikonki
        */
        renderer.setIcon(iconPatient);
        return renderer;
    }
    
}
