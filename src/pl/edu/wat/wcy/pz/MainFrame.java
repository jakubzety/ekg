/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import net.iharder.dnd.FileDrop;
import pl.edu.wat.wcy.pz.Datas.EKGCsvReader;
import pl.edu.wat.wcy.pz.Datas.EKGCsvWriter;
import pl.edu.wat.wcy.pz.Datas.EKGData;

/**
 *
 * @author Jakub
 */
public class MainFrame extends javax.swing.JApplet {
      
    public static void main(String[] args) {
        GlobalFrame = new JFrame();
        GlobalFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainFrame app = new MainFrame();
        app.setSize(550, 370);
        app.init();
        app.start();
        GlobalFrame.setSize(560, 480);
        GlobalFrame.getContentPane().add(app);
        GlobalFrame.repaint();
        app.setVisible(true);
        GlobalFrame.setVisible(true);
    }

    public MainFrame() {
        super();        
    }

    /**
     * Initializes the applet MainFrame
     */
    @Override
    public void init() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        try {
            /* Create and display the applet */
            java.awt.EventQueue.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    initComponents();
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }   
                
        jFrame1.setSize(400,300);
        jFrame1.setVisible(true);
        jFrame1.pack();
        
        pnlView.addPropertyChangeListener("width",new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                int change = (int)evt.getOldValue() - (int)evt.getNewValue();
                graphViewer.setSize(graphViewer.getWidth()+change, graphViewer.getHeight());
                graphSliderX.setSize(graphSliderX.getWidth()+change, graphSliderX.getHeight());
            }
        });
        new  FileDrop( graphViewer, new FileDrop.Listener()
        {   
            @Override
            public void  filesDropped(File[] files ){
                if (files == null) return;
                if(files.length==1){
                    if(files[0].getName().endsWith(".csv")){
                        graphViewer.erraseData();
                        graphViewer.putData(EKGCsvReader.readData(files[0]));
                        graphSliderX.setEnabled(true);
                        graphViewer.setEnabled(true);
                    }
                }
            }
        });
        ChangeListener chListminZero = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if((int)((javax.swing.JSpinner)e.getSource()).getValue()<0){
                    ((javax.swing.JSpinner)e.getSource()).setValue(0);
                }
            }
        };
        spnSetsSplitXNum.addChangeListener(chListminZero);
        spnSetsSplitYNum.addChangeListener(chListminZero);
        spnSetsMargineNorth.addChangeListener(chListminZero);
        spnSetsMargineEast.addChangeListener(chListminZero);
        spnSetsMargineWest.addChangeListener(chListminZero);
        spnSetsMargineSouth.addChangeListener(chListminZero);  
    }

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jFrame1 = new javax.swing.JFrame();
        frmBaseCotrol = new javax.swing.JFrame();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        frmSettings = new javax.swing.JFrame();
        pnlGraphViewSets = new javax.swing.JPanel();
        lblSetsGridCol = new javax.swing.JLabel();
        lblStesAroundCol = new javax.swing.JLabel();
        lblSetsBCGCol = new javax.swing.JLabel();
        lblSetsPlotCol = new javax.swing.JLabel();
        lblSetsMarkCol = new javax.swing.JLabel();
        cbxSetsColorGrid = new javax.swing.JComboBox(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors);
        cbxSetsColorAround = new javax.swing.JComboBox(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors);
        cbxSetsColorBCKGRND = new javax.swing.JComboBox(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors);
        cbxSetsColorPlot = new javax.swing.JComboBox(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors);
        cbxSetsColorMark = new javax.swing.JComboBox(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors);
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        spnSetsSplitXNum = new javax.swing.JSpinner();
        spnSetsSplitYNum = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        spnSetsMargineNorth = new javax.swing.JSpinner();
        spnSetsMargineEast = new javax.swing.JSpinner();
        spnSetsMargineWest = new javax.swing.JSpinner();
        spnSetsMargineSouth = new javax.swing.JSpinner();
        pnlOthersSets = new javax.swing.JPanel();
        bttnSetsApply = new javax.swing.JButton();
        bttnSetsCancel = new javax.swing.JButton();
        jFileChooser1 = new javax.swing.JFileChooser();
        layeredPane = new javax.swing.JLayeredPane();
        graphViewer = new pl.edu.wat.wcy.pz.Components.GraphView();
        graphSliderX = new javax.swing.JSlider();
        pnlView = new pl.edu.wat.wcy.pz.Components.RollPane();
        lblScaleX = new javax.swing.JLabel();
        txtfldScaleX = new javax.swing.JTextField();
        lblScaleY = new javax.swing.JLabel();
        txtfldScaleY = new javax.swing.JTextField();
        lblPositionY = new javax.swing.JLabel();
        txtfldPositionY = new javax.swing.JTextField();
        bttnAnalyze = new javax.swing.JButton();
        bttnAutoscale = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuFileLoad = new javax.swing.JMenuItem();
        menuFileSave = new javax.swing.JMenuItem();
        menuFileErase = new javax.swing.JMenuItem();
        menuBase = new javax.swing.JMenu();
        menuBaseLoad = new javax.swing.JMenuItem();
        menuBaseSave = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        menuEditSettings = new javax.swing.JMenuItem();

        jFrame1.setBackground(new java.awt.Color(255, 255, 51));
        jFrame1.setBounds(new java.awt.Rectangle(100, 0, 0, 0));
        jFrame1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 324, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 253, Short.MAX_VALUE)
        );

        jLabel7.setText("Pacjenci:");

        jLabel8.setText("Przebiegi:");

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.setCellRenderer(new pl.edu.wat.wcy.pz.Components.others.PatientCellRenderer());
        jList1.setValueIsAdjusting(true);
        jScrollPane1.setViewportView(jList1);

        jList2.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList2);

        jButton1.setText("Zaniechaj");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Wybierz");

        javax.swing.GroupLayout frmBaseCotrolLayout = new javax.swing.GroupLayout(frmBaseCotrol.getContentPane());
        frmBaseCotrol.getContentPane().setLayout(frmBaseCotrolLayout);
        frmBaseCotrolLayout.setHorizontalGroup(
            frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frmBaseCotrolLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(frmBaseCotrolLayout.createSequentialGroup()
                        .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frmBaseCotrolLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        frmBaseCotrolLayout.setVerticalGroup(
            frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frmBaseCotrolLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(frmBaseCotrolLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pnlGraphViewSets.setBorder(javax.swing.BorderFactory.createTitledBorder("Wykres"));

        lblSetsGridCol.setText("Kolor siatki");

        lblStesAroundCol.setText("Kolor osi");

        lblSetsBCGCol.setText("Kolor tła");

        lblSetsPlotCol.setText("Kolor wykresu");

        lblSetsMarkCol.setText("Kolor punktu");

        cbxSetsColorGrid.setEditable(true);
        cbxSetsColorGrid.setRenderer(new pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer());
        cbxSetsColorGrid.setEditor(new pl.edu.wat.wcy.pz.Components.others.ColorComboBoxEditor(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors[0]));

        cbxSetsColorAround.setEditable(true);
        cbxSetsColorAround.setRenderer(new pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer());
        cbxSetsColorAround.setEditor(new pl.edu.wat.wcy.pz.Components.others.ColorComboBoxEditor(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors[0]));

        cbxSetsColorBCKGRND.setEditable(true);
        cbxSetsColorBCKGRND.setRenderer(new pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer());
        cbxSetsColorBCKGRND.setEditor(new pl.edu.wat.wcy.pz.Components.others.ColorComboBoxEditor(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors[0]));

        cbxSetsColorPlot.setEditable(true);
        cbxSetsColorPlot.setRenderer(new pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer());
        cbxSetsColorPlot.setEditor(new pl.edu.wat.wcy.pz.Components.others.ColorComboBoxEditor(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors[0]));

        cbxSetsColorMark.setEditable(true);
        cbxSetsColorMark.setRenderer(new pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer());
        cbxSetsColorMark.setEditor(new pl.edu.wat.wcy.pz.Components.others.ColorComboBoxEditor(pl.edu.wat.wcy.pz.Components.others.ColorCellRenderer.colors[0]));

        jLabel1.setText("Liczba podziałek na osi X");

        jLabel2.setText("Liczba podziałek na osi Y");

        jLabel3.setText("Górny margines");

        jLabel4.setText("Prawy margines");

        jLabel5.setText("Lewy margines");

        jLabel6.setText("Dolny margines");

        javax.swing.GroupLayout pnlGraphViewSetsLayout = new javax.swing.GroupLayout(pnlGraphViewSets);
        pnlGraphViewSets.setLayout(pnlGraphViewSetsLayout);
        pnlGraphViewSetsLayout.setHorizontalGroup(
            pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGraphViewSetsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblStesAroundCol)
                    .addComponent(lblSetsBCGCol)
                    .addComponent(lblSetsPlotCol)
                    .addComponent(lblSetsMarkCol)
                    .addComponent(lblSetsGridCol))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbxSetsColorGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxSetsColorAround, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxSetsColorBCKGRND, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxSetsColorPlot, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxSetsColorMark, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spnSetsMargineSouth, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(spnSetsSplitYNum)
                    .addComponent(spnSetsSplitXNum, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spnSetsMargineNorth)
                    .addComponent(spnSetsMargineEast)
                    .addComponent(spnSetsMargineWest))
                .addGap(81, 81, 81))
        );
        pnlGraphViewSetsLayout.setVerticalGroup(
            pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGraphViewSetsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlGraphViewSetsLayout.createSequentialGroup()
                        .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSetsGridCol)
                            .addComponent(cbxSetsColorGrid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblStesAroundCol)
                            .addComponent(cbxSetsColorAround, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlGraphViewSetsLayout.createSequentialGroup()
                        .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(spnSetsSplitXNum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(spnSetsSplitYNum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSetsBCGCol)
                    .addComponent(cbxSetsColorBCKGRND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnSetsMargineNorth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSetsPlotCol)
                    .addComponent(cbxSetsColorPlot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnSetsMargineEast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSetsMarkCol)
                    .addComponent(cbxSetsColorMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spnSetsMargineWest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlGraphViewSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(spnSetsMargineSouth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)))
        );

        pnlOthersSets.setBorder(javax.swing.BorderFactory.createTitledBorder("Inne"));

        javax.swing.GroupLayout pnlOthersSetsLayout = new javax.swing.GroupLayout(pnlOthersSets);
        pnlOthersSets.setLayout(pnlOthersSetsLayout);
        pnlOthersSetsLayout.setHorizontalGroup(
            pnlOthersSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pnlOthersSetsLayout.setVerticalGroup(
            pnlOthersSetsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 102, Short.MAX_VALUE)
        );

        bttnSetsApply.setText("Zastosuj");
        bttnSetsApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSetsApplyActionPerformed(evt);
            }
        });

        bttnSetsCancel.setText("Zaniechaj");
        bttnSetsCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSetsCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout frmSettingsLayout = new javax.swing.GroupLayout(frmSettings.getContentPane());
        frmSettings.getContentPane().setLayout(frmSettingsLayout);
        frmSettingsLayout.setHorizontalGroup(
            frmSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlGraphViewSets, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlOthersSets, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, frmSettingsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bttnSetsApply)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bttnSetsCancel)
                .addContainerGap())
        );
        frmSettingsLayout.setVerticalGroup(
            frmSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(frmSettingsLayout.createSequentialGroup()
                .addComponent(pnlGraphViewSets, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(pnlOthersSets, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(frmSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bttnSetsApply)
                    .addComponent(bttnSetsCancel))
                .addContainerGap())
        );

        jFileChooser1.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        jFileChooser1.setDialogTitle("");
        jFileChooser1.setFileFilter(new FileFilter(){
            @Override
            public boolean accept(File f) {
                return f.getName().endsWith(".csv")||f.isDirectory();
            }
            @Override
            public String getDescription() {
                return "pliki CSV";
            }
        });
        jFileChooser1.setPreferredSize(new java.awt.Dimension(560, 397));
        jFileChooser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1ActionPerformed(evt);
            }
        });

        graphViewer.setEnabled(false);

        javax.swing.GroupLayout graphViewerLayout = new javax.swing.GroupLayout(graphViewer);
        graphViewer.setLayout(graphViewerLayout);
        graphViewerLayout.setHorizontalGroup(
            graphViewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        graphViewerLayout.setVerticalGroup(
            graphViewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        graphSliderX.setMaximum(1000);
        graphSliderX.setValue(0);
        graphSliderX.setEnabled(false);
        graphSliderX.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                graphSliderXStateChanged(evt);
            }
        });

        lblScaleX.setText("Skala X:");

        txtfldScaleX.setColumns(10);
        txtfldScaleX.setToolTipText("");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, graphViewer, org.jdesktop.beansbinding.ELProperty.create("${scaleX}"), txtfldScaleX, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblScaleY.setText("Skala Y:");

        txtfldScaleY.setColumns(10);
        txtfldScaleY.setToolTipText("");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, graphViewer, org.jdesktop.beansbinding.ELProperty.create("${scaleY}"), txtfldScaleY, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        lblPositionY.setText("Przesunięcie Y:");

        txtfldPositionY.setColumns(10);
        txtfldPositionY.setToolTipText("");
        txtfldPositionY.setAutoscrolls(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, graphViewer, org.jdesktop.beansbinding.ELProperty.create("${positionY}"), txtfldPositionY, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        bttnAnalyze.setText("Analizuj");
        bttnAnalyze.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnAnalyzeActionPerformed(evt);
            }
        });

        bttnAutoscale.setText("Autoskalowanie");
        bttnAutoscale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnAutoscaleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlViewLayout = new javax.swing.GroupLayout(pnlView);
        pnlView.setLayout(pnlViewLayout);
        pnlViewLayout.setHorizontalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtfldScaleX)
                    .addComponent(txtfldScaleY)
                    .addComponent(txtfldPositionY)
                    .addComponent(bttnAnalyze, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bttnAutoscale, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(lblScaleX, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblScaleY, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPositionY, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlViewLayout.setVerticalGroup(
            pnlViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlViewLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblScaleX)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtfldScaleX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblScaleY)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtfldScaleY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 226, Short.MAX_VALUE)
                .addComponent(lblPositionY)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtfldPositionY, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bttnAnalyze)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bttnAutoscale)
                .addContainerGap())
        );

        javax.swing.GroupLayout layeredPaneLayout = new javax.swing.GroupLayout(layeredPane);
        layeredPane.setLayout(layeredPaneLayout);
        layeredPaneLayout.setHorizontalGroup(
            layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(graphViewer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(graphSliderX, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layeredPaneLayout.setVerticalGroup(
            layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layeredPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layeredPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layeredPaneLayout.createSequentialGroup()
                        .addComponent(graphViewer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(graphSliderX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layeredPane.setLayer(graphViewer, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layeredPane.setLayer(graphSliderX, javax.swing.JLayeredPane.DEFAULT_LAYER);
        layeredPane.setLayer(pnlView, javax.swing.JLayeredPane.DEFAULT_LAYER);

        menuFile.setText("Plik");

        menuFileLoad.setText("Załaduj z pliku...");
        menuFileLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuFileLoadActionPerformed(evt);
            }
        });
        menuFile.add(menuFileLoad);

        menuFileSave.setText("Eksportuj do pliku ...");
        menuFileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuFileSaveActionPerformed(evt);
            }
        });
        menuFile.add(menuFileSave);

        menuFileErase.setText("Wyczyść dane");
        menuFileErase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuFileEraseActionPerformed(evt);
            }
        });
        menuFile.add(menuFileErase);

        menuBar.add(menuFile);

        menuBase.setText("Baza danych");

        menuBaseLoad.setText("Załaduj z bazy danych");
        menuBaseLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuBaseLoadActionPerformed(evt);
            }
        });
        menuBase.add(menuBaseLoad);

        menuBaseSave.setText("Zapisz do bazy danych");
        menuBase.add(menuBaseSave);

        menuBar.add(menuBase);

        menuEdit.setText("Edytuj");

        menuEditSettings.setText("Ustawienia...");
        menuEditSettings.setToolTipText("");
        menuEditSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuEditSettingsActionPerformed(evt);
            }
        });
        menuEdit.add(menuEditSettings);

        menuBar.add(menuEdit);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane)
        );

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents

    private void menuFileLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuFileLoadActionPerformed
        jFileChooser1.showOpenDialog(this);
    }//GEN-LAST:event_menuFileLoadActionPerformed

    private void graphSliderXStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_graphSliderXStateChanged
        graphViewer.setPositionXProp(graphSliderX.getValue(), graphSliderX.getMaximum());
    }//GEN-LAST:event_graphSliderXStateChanged

    private void bttnAnalyzeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnAnalyzeActionPerformed
        if (graphViewer.getDatas() != null) {
            graphViewer.setShowPeaks(true);
            graphViewer.repaint();
        }
    }//GEN-LAST:event_bttnAnalyzeActionPerformed

    private void bttnAutoscaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnAutoscaleActionPerformed
        if (graphViewer.getDatas() != null) {
            graphViewer.autoScale();
            repaint();
        }
    }//GEN-LAST:event_bttnAutoscaleActionPerformed

    private void menuFileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuFileSaveActionPerformed
        if (graphViewer.getDatas() != null) {
            jFileChooser1.showSaveDialog(this);
        }
    }//GEN-LAST:event_menuFileSaveActionPerformed

    private void jFileChooser1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser1ActionPerformed
        if(jFileChooser1.getDialogType()== javax.swing.JFileChooser.SAVE_DIALOG){
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    EKGCsvWriter.writeData(jFileChooser1.getSelectedFile().getAbsolutePath(), graphViewer.getDatas());
                    return null;
                }
            };
            worker.execute();
        }else{
            if(jFileChooser1.getSelectedFile()!=null){
                graphViewer.erraseData();
                graphViewer.setEnabled(false);
                graphViewer.setStateTxt("Wczytuję, porsze czekać");
                SwingWorker< EKGData, Void> worker = new SwingWorker<EKGData, Void>() {
                    @Override
                    protected EKGData doInBackground() throws Exception {
                        if (jFileChooser1.getSelectedFile()!=null){
                            return EKGCsvReader.readData(jFileChooser1.getSelectedFile());
                        }else{
                            return null;
                        }
                    }
                    @Override
                    protected void done() {
                        try {
                            if(this.get()!=null){
                                graphViewer.putData(this.get());
                                graphSliderX.setEnabled(true);
                                graphViewer.setEnabled(true);
                                graphViewer.setStateTxt("Wczytano");
                            }
                        } catch (InterruptedException | ExecutionException ex) {
                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                };
                worker.execute();
            }
        }
    }//GEN-LAST:event_jFileChooser1ActionPerformed

    private void menuEditSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuEditSettingsActionPerformed
        frmSettings.setVisible(true);
              
        cbxSetsColorBCKGRND.getEditor().setItem(graphViewer.getColorBCKGRND());
        cbxSetsColorGrid.getEditor().setItem(graphViewer.getColorGrid());
        cbxSetsColorMark.getEditor().setItem(graphViewer.getColorMark());
        cbxSetsColorPlot.getEditor().setItem(graphViewer.getColorPlot());
        cbxSetsColorAround.getEditor().setItem(graphViewer.getColorAround());
        
        spnSetsSplitXNum.setValue(graphViewer.getSplitXNum());
        spnSetsSplitYNum.setValue(graphViewer.getSplitYNum());
        spnSetsMargineNorth.setValue(graphViewer.getMargineNorth());
        spnSetsMargineEast.setValue(graphViewer.getMargineEast());
        spnSetsMargineWest.setValue(graphViewer.getMargineWest());
        spnSetsMargineSouth.setValue(graphViewer.getMargineSouth());       
        
        frmSettings.repaint();
        frmSettings.pack();
    }//GEN-LAST:event_menuEditSettingsActionPerformed

    private void bttnSetsCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSetsCancelActionPerformed
        frmSettings.setVisible(false);
    }//GEN-LAST:event_bttnSetsCancelActionPerformed

    private void bttnSetsApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSetsApplyActionPerformed
        frmSettings.setVisible(false);
        
        graphViewer.setColorBCKGRND((Color)cbxSetsColorBCKGRND.getEditor().getItem());
        graphViewer.setColorGrid((Color)cbxSetsColorGrid.getEditor().getItem());
        graphViewer.setColorMark((Color)cbxSetsColorMark.getEditor().getItem());
        graphViewer.setColorPlot((Color)cbxSetsColorPlot.getEditor().getItem());
        graphViewer.setColorAround((Color)cbxSetsColorAround.getEditor().getItem());
        
        graphViewer.setSplitXNum((int)spnSetsSplitXNum.getValue());
        graphViewer.setSplitYNum((int)spnSetsSplitYNum.getValue());
        graphViewer.setSplitXNum((int)spnSetsSplitXNum.getValue());
        graphViewer.setSplitYNum((int)spnSetsSplitYNum.getValue());
        graphViewer.setMargineNorth((int)spnSetsMargineNorth.getValue());
        graphViewer.setMargineEast((int)spnSetsMargineEast.getValue());
        graphViewer.setMargineWest((int)spnSetsMargineWest.getValue());
        graphViewer.setMargineSouth((int)spnSetsMargineSouth.getValue());
        
        graphViewer.repaint();
    }//GEN-LAST:event_bttnSetsApplyActionPerformed

    private void menuFileEraseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuFileEraseActionPerformed
        graphViewer.erraseData();
        graphViewer.setEnabled(false);
    }//GEN-LAST:event_menuFileEraseActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        frmBaseCotrol.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void menuBaseLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuBaseLoadActionPerformed
        frmBaseCotrol.setVisible(true);
        frmBaseCotrol.pack();        
    }//GEN-LAST:event_menuBaseLoadActionPerformed

    static private javax.swing.JFrame GlobalFrame;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bttnAnalyze;
    private javax.swing.JButton bttnAutoscale;
    private javax.swing.JButton bttnSetsApply;
    private javax.swing.JButton bttnSetsCancel;
    private javax.swing.JComboBox cbxSetsColorAround;
    private javax.swing.JComboBox cbxSetsColorBCKGRND;
    private javax.swing.JComboBox cbxSetsColorGrid;
    private javax.swing.JComboBox cbxSetsColorMark;
    private javax.swing.JComboBox cbxSetsColorPlot;
    private javax.swing.JFrame frmBaseCotrol;
    private javax.swing.JFrame frmSettings;
    private javax.swing.JSlider graphSliderX;
    private pl.edu.wat.wcy.pz.Components.GraphView graphViewer;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList jList1;
    private javax.swing.JList jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLayeredPane layeredPane;
    private javax.swing.JLabel lblPositionY;
    private javax.swing.JLabel lblScaleX;
    private javax.swing.JLabel lblScaleY;
    private javax.swing.JLabel lblSetsBCGCol;
    private javax.swing.JLabel lblSetsGridCol;
    private javax.swing.JLabel lblSetsMarkCol;
    private javax.swing.JLabel lblSetsPlotCol;
    private javax.swing.JLabel lblStesAroundCol;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuBase;
    private javax.swing.JMenuItem menuBaseLoad;
    private javax.swing.JMenuItem menuBaseSave;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenuItem menuEditSettings;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuFileErase;
    private javax.swing.JMenuItem menuFileLoad;
    private javax.swing.JMenuItem menuFileSave;
    private javax.swing.JPanel pnlGraphViewSets;
    private javax.swing.JPanel pnlOthersSets;
    private pl.edu.wat.wcy.pz.Components.RollPane pnlView;
    private javax.swing.JSpinner spnSetsMargineEast;
    private javax.swing.JSpinner spnSetsMargineNorth;
    private javax.swing.JSpinner spnSetsMargineSouth;
    private javax.swing.JSpinner spnSetsMargineWest;
    private javax.swing.JSpinner spnSetsSplitXNum;
    private javax.swing.JSpinner spnSetsSplitYNum;
    private javax.swing.JTextField txtfldPositionY;
    private javax.swing.JTextField txtfldScaleX;
    private javax.swing.JTextField txtfldScaleY;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
