/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Jakub
 */
public class DB {

    private String driverName = "org.sqlite.JDBC";

    private String dbUrl = "jdbc:sqlite:baza.db";

    private Connection conn;

    public DB() throws ClassNotFoundException, SQLException {
        Class.forName(this.driverName);
        this.conn = DriverManager.getConnection(this.dbUrl);
    }

    public ResultSet select(String query) throws ClassNotFoundException, SQLException {
        try {
            Statement stmt = this.conn.createStatement();
            try {
                stmt.setQueryTimeout(30);
                ResultSet rs = stmt.executeQuery(query);

                return rs;
            } finally {
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                conn.close();
            } catch (Exception ignore) {
            }
        }
    }

    public void update(String query) throws ClassNotFoundException, SQLException {
        try {
            Statement stmt = this.conn.createStatement();
            try {
                stmt.setQueryTimeout(30);
                stmt.executeUpdate(query);
            } finally {
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            }
        } finally {
            try {
                conn.close();
            } catch (Exception ignore) {
            }
        }
    }

}
