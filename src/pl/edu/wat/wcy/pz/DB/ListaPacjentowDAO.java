/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.edu.wat.wcy.pz.MainFrame;

/**
 *
 * @author User
 */
public class ListaPacjentowDAO {

    public static List<ResultSet> pobierzListePacjentow() {
        List<ResultSet> ls = new ArrayList<>();
        try {
            DB obj = new DB();
            ResultSet rs = obj.select("SELECT * FROM Pacjenci");
            while (rs.next()) {
                ls.add(rs);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ls;
    }

}
