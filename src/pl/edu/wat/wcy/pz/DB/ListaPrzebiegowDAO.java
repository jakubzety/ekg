/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.wat.wcy.pz.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.edu.wat.wcy.pz.MainFrame;

/**
 *
 * @author User
 */
public class ListaPrzebiegowDAO {

    public static List<ResultSet> pobierzListePrzebiegow(Integer ID_Pacjenta) {
        ResultSet rs = null;
        List<ResultSet> ls = new ArrayList<>();
        try {
            DB obj = new DB();
            if (ID_Pacjenta == null) {
                rs = obj.select("SELECT p.ID_Przebiegu, "
                        + "p.Wartość, "
                        + "p.Czas, "
                        + "pc.Imie AS Imie_pacjenta, "
                        + "pc.Nazwisko AS Nazwisko_pacjenta, "
                        + "pc.Pesel AS Pesel_pacjenta "
                        + "FROM Przebiegi p"
                        + "LEFT JOIN I_Pacjenci_Przebiegi pp ON  p.ID_Przebiegu = pp.ID_Przebiegu"
                        + "LEFT JOIN Pacjenci pc ON pp.ID_Pacjenta = pc.ID"
                        + "ORDER BY p.ID_Przebiegu DESC");
            } else {
                rs = obj.select("SELECT p.ID_Przebiegu, "
                        + "p.Wartość, "
                        + "p.Czas, "
                        + "pc.Imie AS Imie_pacjenta, "
                        + "pc.Nazwisko AS Nazwisko_pacjenta, "
                        + "pc.Pesel AS Pesel_pacjenta "
                        + "FROM Przebiegi p"
                        + "LEFT JOIN I_Pacjenci_Przebiegi pp ON  p.ID_Przebiegu = pp.ID_Przebiegu"
                        + "LEFT JOIN Pacjenci pc ON pp.ID_Pacjenta = pc.ID"
                        + "WHERE pc.ID = " + ID_Pacjenta
                        + "ORDER BY p.ID_Przebiegu DESC");
            }

            while (rs.next()) {
                ls.add(rs);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ls;
    }

    public static List<ResultSet> pobierzPrzebieg(Integer ID_Przebiegu) {
        ResultSet rs = null;

        List<ResultSet> ls = new ArrayList<>();

        try {
            DB obj = new DB();
            rs = obj.select("SELECT p.ID_Przebiegu, "
                    + "p.Wartość, "
                    + "p.Czas, "
                    + "pc.Imie AS Imie_pacjenta, "
                    + "pc.Nazwisko AS Nazwisko_pacjenta, "
                    + "pc.Pesel AS Pesel_pacjenta "
                    + "FROM Przebiegi p"
                    + "LEFT JOIN I_Pacjenci_Przebiegi pp ON  p.ID_Przebiegu = pp.ID_Przebiegu"
                    + "LEFT JOIN Pacjenci pc ON pp.ID_Pacjenta = pc.ID"
                    + "WHERE p.ID_Przebiegu = " + ID_Przebiegu
                    + "ORDER BY p.ID_Przebiegu DESC");

            while (rs.next()) {
                ls.add(rs);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ls;
    }

}
